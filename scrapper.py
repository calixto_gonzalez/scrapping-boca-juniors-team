from datetime import datetime

import bs4
import requests
import string
from dateutil.parser import parse
from urllib.request import urlopen, Request
from bs4 import BeautifulSoup
from dateutil.relativedelta import relativedelta
from dateutil.utils import today

dateNow=datetime.now()

url="http://www.bdfa.com.ar/plantel-Boca-Juniors-6.html"
request=Request(url)
request.add_header('User-agent','Mozilla/5.0')
resp = urlopen(request).read()


listJugadores = []
listFechas = []
listAge=[]
listCountries=[]
listPlayersDefinitive = []

page_soup=BeautifulSoup(resp,"html.parser")
body=page_soup.body

filename="equipo.csv"
f=open(filename,"w")
headers="ID, LASTNAME, NAME, AGE, COUNTRIES\n"
f.write(headers)

def appendsListsPlayersDatesAndCountry():
    jugadores = page_soup.body.select("td[width*=38%]")
    fechaDeNacimiento = page_soup.body.select("td[width*=12%]")
    for jugador in jugadores:
        listJugadores.append(jugador.text)
    for fecha in fechaDeNacimiento:
        listFechas.append(fecha.text)


def processPlayersToCSV():
    for jugador in listPlayersDefinitive:
        f.write(str(jugador[0]) + ",")
        f.write(str(jugador[1].strip()) + ",")
        f.write(str(jugador[2].strip()) + ",")
        f.write(str(jugador[3]) + ",")
        f.write(str(jugador[4]))
        f.write("\n")


def appendsListCountries():
    listita = page_soup.body.select("img[alt*=]")
    for value in listita:
        if value["alt"] != "":
            listCountries.append(value["alt"])



def appendsListAges():
    for fecha in listFechas:
        fechaSingular = parse(fecha)
        age = relativedelta(dateNow, fechaSingular)
        listAge.append(age.years)


def appendsEverthing():
    for index , value in enumerate(listJugadores):
        var = value.split(',')
        listPlayersDefinitive.append([index, string.capwords(var[0].lower()),string.capwords(var[1].strip().lower()), listAge[index], listCountries[index]])


appendsListsPlayersDatesAndCountry()
appendsListAges()
appendsListCountries()
appendsEverthing()
processPlayersToCSV()







f.close()









# # print(parse(listFechas[0]))
#
# fechaParsing=parse(listFechas[0])
# # birthday = datetime.date(fechaParsing)
# birthday = fechaParsing
#
# age = relativedelta(dateNow, birthday)
# print(age.years)
# print(type(birthday))
# print(type(dateNow))
# # calculate age
# # print(age)
# # print("years: ", age.years, " months: ", age.months, " day: ", age.days)
#
#
